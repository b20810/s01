package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// It will tell springboot that this application will function as an endpoint handling web requests
public class S1Application {
	// Annotations in Java Springboot marks classes for their intended functionalities
	// Springboot upon startup scans for classes and assign behaviors based on their annotation
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	// @GetMapping will map a route of an endpoint to access or run our method.
	// When http://localhost:8080/hello is accessed from a client, we will be able to run the hi() method.
	@GetMapping("/hello")
	public String hello(){
		return "Hi! from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name",defaultValue = "John") String nameParameter){
		// Pass data through the URL using our string query
		// http://localhost:8080/hi?name=Joe
		// We retrieve the value of the field "name" from our URL
		// defaultValue is the fallback value when the query string or request param is empty.
		// System.out.println(nameParameter);
		return "Hi! My name is " + nameParameter;
	}

	@GetMapping("/myFavoriteFood")
	public String myFavoriteFood(@RequestParam(value = "food",defaultValue = "Sinigang") String foodParameter){
		return "Hi! My favorite food is " + foodParameter;
	}

	// 2 ways of passing data through the URL by using Java Springboot.
	// Query String using request params - Directly passing data to the url and getting the data
	// Path Variable - much more similar to ExpressJs' req.params

	@GetMapping("/greeting/{name}")
	public String greeting(@PathVariable("name") String nameParams){
		return "Hello " + nameParams;
	}

	// 1. Create an Array for Strings called enrollees
	ArrayList<String> enrollees = new ArrayList<>();

	// 2. Create /enroll route
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user",defaultValue = "null") String enrolleeParams){

		if(enrolleeParams.equals("null")){
			return "User cannot be empty";
		};

		enrollees.add(enrolleeParams);
		return "Welcome, " + enrolleeParams + "!";
	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String getCourseDetails(@PathVariable("id") String courseParams) {
		System.out.println(courseParams);

		switch (courseParams) {
			case "java101":
				return "Name: " + courseParams + "Schedule: MWF 8:00 AM - 11:00AM, Price: Php 5000";
			case "sql101":
				return "Name: " + courseParams + "Schedule: MWF 1:00 PM - 5:00PM, Price: Php 3000";
			case "jsoop101":
				return "Name: " + courseParams + "Schedule: MWF 1:00 PM - 5:00PM, Price: Php 4000";
			default:
				return "Course cannot be found.";
		}
	}




}
